import React, { useMemo } from 'react'

export default function Footer() {
  const currentYear = useMemo(() => (new Date().getFullYear()), [])

  if(!currentYear) return null

  return (
    <>
      <footer>
        <div className="inner">
          <section>
            <ul className="social-links" role="navigation">
              <li><a href=""><i className="fab fa-github"></i></a></li>
              <li><a href=""><i className="fab fa-linkedin-in"></i></a></li>
              <li><a href=""><i className="fab fa-twitter"></i></a></li>
            </ul>
            &copy;<span>{currentYear}</span> Uche Chilaka
          </section>
        </div>
      </footer>
    </>    
  )
}