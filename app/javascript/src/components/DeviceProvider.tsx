import React, { useMemo } from 'react'
import { ReactNode } from 'react'
import { 
  isMobile, 
  isSafari, 
  isFirefox, 
  isIE, 
  isIOS,
  isTablet,
  isAndroid, 
  isBrowser, 
  isChrome, 
  isEdge,
  isEdgeChromium,
  isChromium 
} from 'react-device-detect'
import { DeviceState } from '../interfaces'
import { DeviceContext } from '../lib'

type Props = Partial<DeviceState> & {
  children?: ReactNode
}

/**
 * Evaluate device state and publish as app-accessible context. 
 * @Todo explore what a pub/sub approach looks like for these browser attributes
 * 
 * @param param0 
 */
export default function DeviceProvider({ children, ...props }: Props) {
  const bodyClasses = useMemo(() => [
    isBrowser && 'is-desktop',
    isMobile && 'is-mobile',
    isChromium && 'is-chromium',
    isAndroid && 'is-android',
    isIOS && 'is-ios',
    isEdge && 'is-edge'    
  ].filter(name => name), [isBrowser])

  const defaultValue: DeviceState = {
    bodyClasses,
    isSafari, isFirefox,
    isMobile, isIOS, isAndroid,
    isTablet,
    isBrowser,
    isChrome, isChromium,
    isIE, isEdge, isEdgeChromium,
  }

  return (
    <DeviceContext.Provider 
      value={{ ...defaultValue, ...props }}>
      {children}
    </DeviceContext.Provider>    
  )
}