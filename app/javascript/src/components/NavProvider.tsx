import React, { ReactNode } from 'react'
import { NavState } from '../interfaces'
import { NavContext } from '../lib'

type Props = Partial<NavState> & {
  children?: ReactNode
}

const navStateDefaults: NavState = {
  overlayNavIsVisible: false
}

/**
 * Provider for app navigation context
 * 
 * @param param0 
 */
export default function NavProvider({ children, ...providerProps }: Props) {
  return (
    <NavContext.Provider 
      value={{ ...navStateDefaults, ...providerProps }}>
      {children}
    </NavContext.Provider>
  )
}
