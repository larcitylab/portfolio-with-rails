import React, { 
  useState, 
  useCallback, 
  MouseEvent, 
} from 'react'
import NavLinks from './NavLinks'

export default function TopNav() {
  const [menuIsOpen, setMenuIsOpen] = useState<boolean>(false)

  /**
   * @Todo Feature for toggling mobile responsive menu
   */
  const toggleMenu = useCallback((ev: MouseEvent) => {
    ev.persist()
    ev.preventDefault()
    setMenuIsOpen(!menuIsOpen)
  }, [])

  return (
    <nav id="top-nav">
      <NavLinks toggleMenuCallback={toggleMenu} />
      <div>Uche Chilaka</div>
    </nav>
  )
}
