import React, { MouseEventHandler, ReactNode } from 'react'
import { Link } from 'react-router-dom'

type Props = {
  toggleMenuCallback?: MouseEventHandler,
  menuComponent?: ReactNode
}

export default function NavLinks({ 
  toggleMenuCallback,
  menuComponent
}: Props) {
  return (
    <ul id="top-nav-links">
      {toggleMenuCallback && (
        <li className="toggle-menu-item">
          <a href="#" onClick={toggleMenuCallback}>
            {menuComponent ?? <span>Menu</span>}
          </a>
        </li>
      )}
      <li><Link to="/">Welcome</Link></li>
      <li><Link to="/about">About</Link></li>
      <li><Link to="/portfolio">Portfolio</Link></li>
      <li><a href="http://uchechilaka.com" rel="noopener noreferrer" target="_blank">Blog</a></li>
    </ul>
  )
}