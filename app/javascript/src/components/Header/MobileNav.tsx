import React from 'react'
import NavLinks from './NavLinks'

export default function MobileNav() {
  return (
    <nav id="mobile-nav">
      {NavLinks}
    </nav>
  )
}
