export { default as TopNav } from './TopNav'
export { default as NavLinks } from './NavLinks'

import React from 'react'
import styled from 'styled-components'
import TopNav from './TopNav'
import MobileNav from './MobileNav'

// An example of co-locating SCSS styles with a component, as well as the components React members
import './header.scss'

const StyledHeader = styled.header`
  display: block;
  width: 100vw;
`

/**
 * The header component, co-located with its members
 */
export default function Header() {
  return (
    <>
      <StyledHeader>
        <TopNav />
      </StyledHeader>
      <MobileNav />
    </>
  )
}
