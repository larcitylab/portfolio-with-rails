import { request } from 'gaxios'
import {Project} from "../interfaces";

export default async function getProjects(): Promise<Project[]> {
    const res = await request<Project[]>({ url: '/api/v1/projects' })
    return res.data
}
