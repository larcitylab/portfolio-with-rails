import { useState, useCallback, useEffect } from 'react'
import { request } from 'gaxios'
import {Project} from "../interfaces";

export default function useProjects() {
    const [isLoading, setIsLoading] = useState<boolean>(true)
    const [projects, setProjects] = useState<Project[]>([])

    const getProjects = useCallback(async () => {
        const res = await request<Project[]>({ url: '/api/v1/projects' })
        setProjects(res.data)
        setIsLoading(false)
    }, [])

    useEffect(() => {
      getProjects()
    }, [])

    return {
        isLoading,
        getProjects,
        projects
    }
}