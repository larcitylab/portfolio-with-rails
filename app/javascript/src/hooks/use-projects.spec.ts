import { act, renderHook } from '@testing-library/react-hooks'
import useProjects from './use-projects'

jest.mock('gaxios')

describe('useProjects', () => {
  beforeEach(() => {
    jest.useFakeTimers()
  })

  test('fetches projects', async () => {
    const { result, waitForNextUpdate } = renderHook(() => useProjects())

    await waitForNextUpdate({ timeout: 2000 })

    const { isLoading, projects } = result.current

    expect(isLoading).toBe(false)
    expect(projects).toBeInstanceOf(Array)
    expect(projects.length).toBeGreaterThan(0)
  })
})