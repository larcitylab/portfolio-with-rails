import { useEffect, useState, useCallback } from 'React'

/**
 * Capture current dimensions of specific components to make them
 * available for downstream use-cases like dependent styling. 
 * @Todo what does a Pub/Sub pattern for this look like?
 */
export default function useDimensions(topElement: Document | HTMLElement = document) {
  const [headerDims, setHeaderRect] = useState<DOMRect>()
  const [mainDims, setMainRect] = useState<DOMRect>()

  const header = topElement.querySelector('header')
  const main = topElement.querySelector('main')
  const headerRect = header.getClientRects().item(0)
  const mainRect = main.getClientRects().item(0)

  const getHeaderDims = useCallback(() => {
    setHeaderRect(headerRect)
  }, [headerRect])

  const getMainDims = useCallback(() => {
    setMainRect(mainRect)
  }, [mainRect])

  useEffect(() => {
    getHeaderDims()
    getMainDims()
  }, [headerRect, mainRect])

  return { headerDims, mainDims }
}