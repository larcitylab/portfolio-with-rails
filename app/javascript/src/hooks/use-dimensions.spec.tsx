import React from 'react'
import { act, render, screen } from '@testing-library/react'
import { renderHook } from '@testing-library/react-hooks'
import useDims from './use-dimensions'
import MockMain from '../../test/components/MockMain'

describe('useDimensions', () => {
  beforeEach(async () => {
    act(() => {
      render(<MockMain />)
    })
  })

  describe('main', () => {
    let dims = null

    beforeEach(async () => {
      const { result, waitForNextUpdate } = renderHook(
        () => useDims()
      )
      // await waitForNextUpdate()
      dims = result.current.mainDims
    })

    // @Todo Resolve error from a createElement failure on render
    xit('captures dimensions correctly', () => {
      console.debug(dims)
    })
  })
})