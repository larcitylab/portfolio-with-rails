export interface Project {
    permalink: string
    uri: string
    image_url: string
    title: string
}

export interface DeviceState {
  bodyClasses: string[]
  isIE?: boolean
  isSafari?: boolean
  isFirefox?: boolean
  isIOS?: boolean
  isAndroid?: boolean
  isTablet?: boolean
  isBrowser?: boolean
  isChrome?: boolean
  isChromium?: boolean
  isEdge?: boolean
  isEdgeChromium?: boolean
  isMobile?: boolean
  [key: string]: any
}

export interface NavState {
  overlayNavIsVisible: boolean
}