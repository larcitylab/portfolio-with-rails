import React, { useContext, useEffect } from 'react'
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Welcome, About, Portfolio } from "./views";
import { Footer, Header, NavProvider } from "./components";
import { DeviceContext } from './lib/contexts'

import "./styles/app.scss"

export default function App() {
  const { bodyClasses } = useContext(DeviceContext)

  useEffect(() => {
    const body = document.querySelector('body')
    body.classList.add(...bodyClasses)
  }, [])

  return (
    <NavProvider>
      <Router>
        {/* @important the header MUST be rendered within the router - uses ReactRouterDom.Link component */}
        <Header />
        <Switch>
          <Route path="/about">
            <About />
          </Route>
          <Route path="/portfolio">
            <Portfolio />
          </Route>
          <Route exact path="/">
            <Welcome />
          </Route>
        </Switch>
        <Footer />
      </Router>
    </NavProvider>
  );
}