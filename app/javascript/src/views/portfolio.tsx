import React from 'react'
import { Project } from '../interfaces'

export default function Portfolio() {
    console.debug('Loading Portfolio')

    const projects: Project[] = []

    return (
        <>
          <div id="portfolio" className="main">
            <section className="banner">
              <div className="inner">
                <h1>Portfolio</h1>
              </div>
            </section>
            <section>
              <ul id="showcase">
                {projects.map((project) => {
                  return (
                    <li>
                      <img src={project.image_url} alt={`screenshot of ${project.title}`} />
                      <h3>{project.title}</h3>
                    </li>
                  )
                })}
              </ul>
            </section>
          </div>
        </>
    )
}
