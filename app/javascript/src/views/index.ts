export { default as Welcome } from './welcome'
export { default as About } from './about'
export { default as Portfolio } from './portfolio'
