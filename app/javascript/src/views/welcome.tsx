import React from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

const StyledSection = styled.section`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-content: center;

  .inner {
    /* width: inherit; */
    display: flex;
    flex-direction: column;
    align-items: flex-start;

    h1, h3 {
      display: inline-block;
      text-align: inherit;
    }
  }
`

const StyledVideo = styled.video`
  z-index: -1;
  top: 0; left: 0;
  display: block;
  min-width: 100vw;
  min-height: 100vh;
  overflow: hidden;
  height: auto;
  position: fixed;
`

const VIDEO_URL = 'https://storage.googleapis.com/com-larcity-shared/videos/Animated%20Rocket%20by%20Motion%20Stacks.mp4'

export default function Welcome() {
  return (
    <>
      <StyledSection id="welcome" className="inner template-body">
        <section className="inner">
          <h1>My name is <span className="highlighted">Uche</span><br /> and I love to make things</h1>
          <h3>
            A few other things about me:
            <ul>
              <li>I love tinkering and making stuff</li>
              <li>I enjoy solving problems</li>
              <li>My pronouns are He/Him</li>
            </ul>
          </h3>
          <Link className="button" to="/portfolio">View My Work</Link>
        </section>
      </StyledSection>
      <StyledVideo autoPlay muted loop id="rocket-launch">
        <source src={VIDEO_URL} type="video/mp4" />
      </StyledVideo>
    </>
  )
}
