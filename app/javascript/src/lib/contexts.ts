import React from 'react'
import { DeviceState, NavState } from '../interfaces'

export const DeviceContext = React.createContext<DeviceState>({ bodyClasses: [] })

export const NavContext = React.createContext<NavState>({ overlayNavIsVisible: false })