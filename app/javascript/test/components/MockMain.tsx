import React from 'react'
import styled from 'styled-components'

const StyledMain = styled.main`
header {
  min-height: 122px;
}
main {
  min-height: 244px;
}
`

export default function MockMain() {
  return (
    <StyledMain data-testid="view-root">
      <header>
        <ul>
          <li>First item</li>
          <li>Second item</li>
        </ul>
      </header>
      <section>
        Lorm Ipsum Tadeus
      </section>
    </StyledMain>
  )
}