// Run this example by adding <%= javascript_pack_tag 'default' %> to the head of your layout file,
// like app/views/layouts/application.html.erb.

import React from "react";
import { render } from "react-dom";
import App from "../src/app";
import DeviceProvider from "../src/components/DeviceProvider";

document.addEventListener("DOMContentLoaded", () => {
  console.debug("React with TypeScript support is live");

  render(
    <DeviceProvider>
      <App />
    </DeviceProvider>,
    document.body.appendChild(document.querySelector('main#default'))
  );
});
