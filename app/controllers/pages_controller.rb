class PagesController < ApplicationController
  def index; end

  private

  def setup_projects
    @projects = [
      {
        title: 'Bolt Network',
        image_url: bucket_asset_url(uri: '/learn-rails--bolt-network/Screenshot+from+2020-08-24+03-50-41.png'),
      },
      {
        title: 'Broadway',
        image_url: bucket_asset_url(uri: '/learn-rails--broadway/Screenshot+from+2020-08-24+03-48-07.png'),
      },
      {
        title: 'Innovation Cloud',
        image_url: bucket_asset_url(uri: '/learn-rails--innov-cloud/Screenshot+from+2020-08-24+04-27-41.png'),
      },
      {
        title: 'Threadly',
        image_url: bucket_asset_url(uri: '/learn-rails--threadly/Screenshot+from+2020-08-24+04-29-10.png'),
      },
    ]
  end

  def bucket_asset_url(uri: '')
    "https://s3.us-east-2.amazonaws.com/screenshots.uche.page/projects#{uri}"
  end
end
