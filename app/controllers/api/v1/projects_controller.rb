class Api::V1::ProjectsController < ApplicationController
  def show
    render json: Project.not_deprecated
  end
end
