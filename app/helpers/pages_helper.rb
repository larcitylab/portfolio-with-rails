module PagesHelper
  def rocket_launch_video_url
    'https://storage.googleapis.com/com-larcity-shared/videos/Animated%20Rocket%20by%20Motion%20Stacks.mp4'
  end
end
