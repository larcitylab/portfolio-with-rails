class Project < ApplicationRecord
  scope :not_deprecated, -> { where(deprecated_at: nil) }
end
