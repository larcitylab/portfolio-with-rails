# Portfolio (Rails + React)<!-- omit in toc -->

This codebase is relevant for candidates completing the `Portfolio` portion of the Codecademy `Learning Rails` program.

- [Chapter 1: Setting up Project](#chapter-1-setting-up-project)
  - [Setup Rails + Webpacker + React](#setup-rails--webpacker--react)
  - [Add TypeScript support](#add-typescript-support)
  - [Adding React advanced SPA features](#adding-react-advanced-spa-features)
- [Additional Notes](#additional-notes)
  - [Future Chapter(s)](#future-chapters)
  - [Building Guide Videos](#building-guide-videos)

## Chapter 1: Setting up Project

### Setup Rails + Webpacker + React

This is best done right at the beginning of your project with one command:

```shell
rails new <project-name> --skip-test --webpack=react
```

> Todo: what does it look like to add this after the fact? Idea to create a new project and migrate the code over as the best alternative

### Add TypeScript support

```shell
bundle exec rails webpacker:install:typescript
```

Cherry pick the steps in the [Webpacker documentation on TypeScript](https://github.com/rails/webpacker#typescript) for SCSS support (if you experience errors).

### Adding React advanced SPA features

There's a great quide [published here](https://www.digitalocean.com/community/tutorials/how-to-set-up-a-ruby-on-rails-project-with-a-react-frontend) to get you most of the way.

## Additional Notes

Future content will be organized in chapters. In each Chapter, we'll address:

- The basics - getting up and running
- Alternative approaches and tools
- Rabbit holes - things to look out for
- Chapter updates (if any - cut in on post edits)

### Future Chapter(s)

- Deploy to Heroku & check off Codecademy exercise
- Adding Font Awesome
- Bootstrapping dynamic, external stylesheets
- Adding styled components
- Adding Storybook (for React)
- Adding Swagger Implementation for Rails backend (RSwag)
- Add Local SSL

### Building Guide Videos

- Explore using Pendo for on-screen cues and content overlays
