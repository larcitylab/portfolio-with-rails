import { GaxiosOptions } from "gaxios"

const mockedGAxios = jest.createMockFromModule('gaxios')

const mockRequest = jest.fn(async ({ url, method }: GaxiosOptions) => {
  if(/api\/v1\/projects$/.test(url) && (!method || /get/.test(method))) {
    return {
      data: [
        {
          title: 'Project 1',
          uri: '/test/path/project-1.png',
          permalink: 'project-1'
        },
        {
          title: 'Project 2',
          uri: '/test/path/project-2.png',
          permalink: 'project-2'
        }
      ]
    }
  }

  return {
    data: []
  }
})

export const request = mockRequest

Object.defineProperty(mockedGAxios, 'request', mockRequest)

export default mockedGAxios
