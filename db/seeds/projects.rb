Project.find_or_create_by(permalink: 'bolt-network') do |proj|
  proj.title = 'Bolt Network'
  proj.uri = '/learn-rails--bolt-network/Screenshot+from+2020-08-24+03-50-41.png'
end

Project.find_or_create_by(permalink: 'broadway') do |proj|
  proj.title = 'Broadway'
  proj.uri = '/learn-rails--broadway/Screenshot+from+2020-08-24+03-48-07.png'
end

Project.find_or_create_by(permalink: 'innov-cloud') do |proj|
  proj.title = 'Innovation Cloud'
  proj.uri = '/learn-rails--innov-cloud/Screenshot+from+2020-08-24+04-27-41.png'
end

Project.find_or_create_by(permalink: 'threadly') do |proj|
  proj.title = 'Threadly'
  proj.uri = '/learn-rails--threadly/Screenshot+from+2020-08-24+04-29-10.png'
end
