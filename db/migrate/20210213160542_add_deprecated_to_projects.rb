class AddDeprecatedToProjects < ActiveRecord::Migration[6.0]
  def change
    add_column :projects, :deprecated_at, :datetime
  end
end
