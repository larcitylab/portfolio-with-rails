class AddPermalinkToProjects < ActiveRecord::Migration[6.0]
  def change
    add_column :projects, :permalink, :string
    add_column :projects, :uri, :string
  end
end
