Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resource :projects
    end
  end

  # React routing
  root 'pages#index'
  get '/*path', to: 'pages#index'
end
